package com.example.home.validator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.login)
    EditText login;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.password)
    EditText pass;
    @BindView(R.id.confirm)
    EditText confirm;
    @BindView(R.id.textView)
    TextView textView;
    @BindView(R.id.validate)
    Button button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.validate)
    public void validate() {
        textView.setText(null);
        if (login.length() == 0) {
            textView.setText(R.string.login_err);
        }
        if (!isValidEmail(email.getText())) {
            textView.setText(R.string.email_err);
        }
        if (!isValidPhoneNumber(phone.getText())) {
            textView.setText(R.string.phone_err);
        }
        boolean check = String.valueOf(pass.getText()).equals(String.valueOf(confirm.getText()));
        if (!check) {
            textView.setText(R.string.pas_mach_err);
        }
        if (pass.length() == 0) {
            textView.setText(R.string.pass_err);
        }
        if (confirm.length() == 0) {
            textView.setText(R.string.confirm_err);
        }


    }

    private boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private boolean isValidPhoneNumber(CharSequence target) {
        return target != null && target.length() >= 6 && target.length() <= 13 && android.util.Patterns.PHONE.matcher(target).matches();

    }

}
